<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Register</title>
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="  {{ asset('css/register.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css">

</head>

<body class="body">
<div class="section-register">
    <div class="img-logo">
    </div>
    <h3 class="header-register">Please sign in to continue</h3>
    <form method="POST" action="" id="loginForm" class="w-100">
        <div class="col-xl-12">
            <div class="form-group  mr-2">
                <label for="username">Login</label><br>
                <input type="text" id="username" name="username" class="form-control w-100"><br>
            </div>

            <div class="form-group">
                <label for="password">Password:</label><br>
                <input type="password" id="password" name="password" class="form-control w-100"><br>
            </div>

            <div class="form-group text-center">
                <button class="btn btn-primary" type="button" class="mt-2" id="loginBtn" name="loginGroup" onclick="login()">
                    Login
                </button>
            </div>
        </div>
    </form>
    <div class="form-group text-center">
    <a href="{{ url('register') }}" class="header-register">Create account</a>
    </div>
</div>


<script>

    function login() {
        const loginFormBlock = document.querySelector('#loginForm');

        const urlPath = "{{ route('login') }}";
        const allInputs = loginFormBlock.querySelectorAll('input[type=text], input[type=password]');

        const objForSent = {};
        const allVisibleInputs = [];

        allInputs.forEach((input) => {
            if (input.offsetParent) {
                allVisibleInputs.push(input);
            }
        });

        allVisibleInputs.forEach((el) => {
                objForSent[el.name] = el.value;
        });

        $.ajax({
            type: "POST",
            url: urlPath,
            data: {
                dataUsers: JSON.stringify(objForSent),
            },
            success: function (response) {
                if (response){
                    if (response.success) {
                        alert(response.message);
                            window.location.href = '{{ route('dashboard') }}';

                    }
                }

            },
            error: function (jqXHR, textStatus, errorThrown) {
                const errorMessage = jqXHR.responseJSON.message;
                alert(errorMessage);
            }
        });
    }

</script>


<!-- Include jQuery -->
<script src=" {{ asset('js/jquery-1.11.1.min.js') }}"></script>
</body>
</html>






