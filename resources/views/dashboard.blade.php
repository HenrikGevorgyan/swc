<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Register</title>
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="  {{ asset('css/register.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css">

</head>

<style>
    .d-flex {
        display: flex;
        justify-content: space-between;
        align-items: center;
        padding: 10px;
        border-bottom: 1px solid #ccc;
    }

    .header-register {
        margin: 0;
        font-size: 24px;
        color: #333;
    }

    .text-end {
        text-align: right;
    }

    .btn-logout {
        display: inline-block;
        padding: 8px 16px;
        font-size: 14px;
        font-weight: bold;
        text-transform: uppercase;
        color: #fff;
        background-color: #007bff;
        border: 1px solid #007bff;
        border-radius: 4px;
        cursor: pointer;
        transition: all 0.3s ease;
    }

    .btn-logout:hover {
        background-color: #0056b3;
        border-color: #0056b3;
    }
</style>

<body class="body">
<div class="d-flex mb-5">
    <div>
        <h3 class="header-register">Dashboard</h3>
    </div>
    <div class="text-end">
        <form id="logoutForm" action="{{ route('logout') }}" method="POST">
            @csrf
            <button type="submit" class="btn btn-outline-primary">Logout</button>
        </form>
    </div>
</div>

<div class="form-group d-flex">
<div class="container ml-0">
    <h2 class="ml-3">Все события</h2>
    <div class="form-group ml-0">
        @foreach($events as $event)
            <div class="col-md-6">
                <div class="card mb-3">
                    <div class="card-body p-2">
                        <h5 class="card-title"><a href="#" onclick="showEvent({{ $event->id }})">{{ $event->title }}  {{ $event->id }}</a></h5>
                        <p class="card-text">{{ $event->text }}</p>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
<div class="container ml-0">
    <h2>Событие</h2>
    <div class="form-group ml-0">

        <h2 id="eventTitle"></h2>
        <p id="eventText"></p>
        <h5>Участники:</h5>
        <ul id="eventUsers"></ul>
        <span type="hidden" id="eventID" value="">
        <button onclick="joinEvent()" class="btn-outline-primary">Принять участие</button>

    </div>
</div>
</div>
<div class="form-group d-flex">
<div class="container ml-4">
    <h2>Мои события</h2>
    <div class="form-group ml-0">

        @foreach($authEvents as $authEvent)
            <div class="col-md-6 pl-0">
                <div class="card mb-3">
                    <div class="card-body p-2">
                        <h5 class="card-title">{{ $authEvent->title }}  {{ $authEvent->id }}</h5>
                        <p class="card-text">{{ $authEvent->text }}</p>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
    <div class="container ml-0"></div>
</div>
<script>
    function showEvent(eventID){
        const urlPath = `{{ route('show', ['id' => ':id']) }}`;
        const formattedUrl = urlPath.replace(':id', eventID);

        $.ajax({
            type: "GET",
            url: formattedUrl,
            success: function (response) {
                if(response){
                    console.log('response',response)
                    if (response.data.event && response.data.event.title && response.data.event.text) {
                        $('#eventTitle').text(response.data.event.title);
                        $('#eventText').text(response.data.event.text);
                        $('#eventID').val(response.data.event.id);
                        let timerInterval;
                    } else {
                        $('#eventTitle').text('Event Title Not Available');
                        $('#eventText').text('Event Text Not Available');
                    }

                    const users = response.data.event_user;
                    if (Array.isArray(users)) {
                        const usersList = users.map(user => `<li>${user.login}</li>`).join('');
                        $('#eventUsers').html(usersList);
                    } else {
                        $('#eventUsers').html('<li>No participants</li>');
                    }
                }

            },
            error: function (jqXHR, textStatus, errorThrown) {
                const errorMessage = jqXHR.responseJSON.message;
                alert(errorMessage);
            }
        });
    }

    function joinEvent(){
        const eventID  = document.querySelector('#eventID').value;
        console.log('eventID',eventID);
        const urlPath = `{{ route('join.event', ['event_id' => ':event_id']) }}`;
        const formattedUrl = urlPath.replace(':event_id', eventID);

        $.ajax({
            type: "POST",
            url: formattedUrl,
            success: function (response) {
                if(response){
                    let timerInterval;
                    Swal.fire({
                        title: "Good job!",
                        text: "Successfully joined the event",
                        html: 'joined <b></b> successfully!',
                        icon: "success",
                        showCancelButton: false,
                        showConfirmButton: false,
                        timer: 1000,
                        timerProgressBar: true,
                        willClose: () => {
                            clearInterval(timerInterval)
                        }
                    });
                }

                showEvent(eventID);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                const errorMessage = jqXHR.responseJSON.message;
                alert(errorMessage);
            }
        });

        console.log('Joining event with ID:', eventID);
    }

</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<!-- Include jQuery -->
<script src=" {{ asset('js/jquery-1.11.1.min.js') }}"></script>
</body>
</html>






