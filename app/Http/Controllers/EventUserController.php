<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\User;
use Illuminate\Http\Request;

class EventUserController extends Controller
{

    /**
     * e) участие в событии;
    */
    public function joinEvent($eventId, Request $request)
    {

        $user = User::find(auth()->id());
        $event = Event::find($eventId);

        if (!$user || !$event) {
            return response()->json([
                'error' => 'User or Event not found.',
            ], 404);
        }

        $event->event_user()->attach($user->id);

        return response()->json([
            'error' => null,
            'message' => 'User joined event successfully.',
        ]);
    }

    /**
     * f) отмена участия в событии;
    */
    public function leaveEvent($eventId, Request $request)
    {
        $user = User::find($request->user_id);
        $event = Event::find($eventId);

        if (!$user || !$event) {
            return response()->json([
                'error' => 'User or Event not found.',
            ], 404);
        }

        $event->event_user()->detach($user->id);

        return response()->json([
            'error' => null,
            'message' => 'User left event successfully.',
        ]);
    }
}
