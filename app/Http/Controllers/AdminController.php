<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class AdminController extends Controller
{
    public function register(){
        return view('register');
    }

    public function login(){
        return view('login');
    }

    public function dashboard(){
        $events = Event::get();
        $authEvents = auth()->user()->event_user;
        return view('dashboard',compact('events','authEvents'));
    }
}
