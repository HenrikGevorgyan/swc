<?php

namespace App\Http\Controllers;

use App\Http\Controllers\API\BaseController;
use App\Models\Event;
use App\Models\User;
use Illuminate\Http\Request;

class EventController extends BaseController
{
    /**
     * d) получение списка событий;
    */
    public function index()
    {
        $events = Event::all();
        return $this->sendResponse($events, 'Displaying all Event data');
    }

    /**
     * c) создание события;
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'title' => 'required',
            'text' => 'required',
            'user_id' => 'required|exists:users,id',
        ]);

        $event = Event::create($validatedData);

        return $this->sendResponse($event, 'Event created successfully.');
    }

    public function show($id)
    {
        $event = Event::find($id);

        if (!$event) {
            return $this->sendError('Event not found.');
        }
        $params = [
            'event' => $event,
            'event_user' => $event->event_user,
        ];

        return $this->sendResponse($params, 'Displaying Event data');
    }

    /**
     * g) удаление события создателем.
     */
    public function destroy($eventId, Request $request)
    {
        $user = User::find($request->user_id);
        $event = Event::find($eventId);

        if (!$user || !$event) {
            return response()->json([
                'error' => 'User or Event not found.',
            ], 404);
        }


        if ($user->id !== $event->user_id) {
            return response()->json([
                'error' => 'You do not have permission to delete this event.',
            ], 403);
        }
        $event->event_user()->detach();
        $event->delete();
        return response()->json([
            'error' => null,
            'message' => 'Event deleted successfully.',
        ]);
    }


}
