<?php

use App\Http\Controllers\API\RegisterController;
use App\Http\Controllers\EventUserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EventController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
// Регистрация пользователя
Route::post('register', [RegisterController::class, 'register'])->name('register');
// Авторизация пользователя
Route::post('login', [RegisterController::class, 'login'])->name('login');
Route::post('logout', [RegisterController::class, 'logout'])->name('logout');

Route::middleware('auth:sanctum')->group(function () {
    //d) получение списка событий;
    Route::get('/events', [EventController::class, 'index']);
    //c) создание события;
    Route::post('/events', [EventController::class, 'store']);
    Route::get('/events/{id}', [EventController::class, 'show'])->name('show');
    Route::prefix('events')->group(function () {
        //g) удаление события создателем.
        Route::delete('/{event_id}', [EventController::class, 'destroy']);
        //e) участие в событии;
        Route::post('/{event_id}/participants', [EventUserController::class, 'joinEvent'])->name('join.event');
        //f) отмена участия в событии;
        Route::delete('/{event_id}/participants', [EventUserController::class, 'leaveEvent']);
    });

});


