## Как собрать проект

- Клонировать проект `git clone`
- Установить [компосер](https://getcomposer.org/download/) и запустить команду в проекте `composer install`
- Копировать `.env.example` в `.env`
- Создать БазуДанных в `phpMyAdmin` и настроить `.env` с доступами к БазеДанных
- Генерировать ключ для APP `php artisan key:generate`
- Запустить миграцию (для создание таблиц в БД) `php artisan migrate`
- Запустить  `php artisan serve`
- Поздравляю! вы собрали проект! приятного пользования!
